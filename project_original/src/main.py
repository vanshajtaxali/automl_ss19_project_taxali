import os
import argparse
import logging
import time
import numpy as np
import torch
import torchvision.transforms as transforms
from torch.utils.data import DataLoader
from torchsummary import summary
import torchvision.models as models
from cnn import torchModel
from datasets import K49,KMNIST
import torch.nn as nn
from matplotlib import pyplot as plt

def main(model_config,
         data_dir,
         num_epochs=10,
         batch_size=50,
         learning_rate=0.001,
         train_criterion=torch.nn.CrossEntropyLoss,
         model_optimizer=torch.optim.Adam,
         data_augmentations=False,
         save_model_str=None,
         load_model_str=None):

    #[transforms.RandomHorizontalFlip(),transforms.RandomRotation(10),transforms.Resize(224),transforms.ToTensor()]
    """
    Training loop for configurableNet.
    :param model_config: network config (dict)
    :param data_dir: dataset path (str)
    :param num_epochs: (int)
    :param batch_size: (int)
    :param learning_rate: model optimizer learning rate (float)
    :param train_criterion: Which loss to use during training (torch.nn._Loss)
    :param model_optimizer: Which model optimizer to use during trainnig (torch.optim.Optimizer)
    :param data_augmentations: List of data augmentations to apply such as rescaling.
        (list[transformations], transforms.Composition[list[transformations]], None)
        If none only ToTensor is used
    :return:
    """

    # Device configuration
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

    if data_augmentations is False:
        # You can add any preprocessing/data augmentation you want here
        data_augmentations=transforms.ToTensor()
      
    elif data_augmentations is True:
        print('Data augmentation :TRUE')
       # data_augmentations = transforms.Compose([transforms.ToPILImage(mode=None),transforms.ToTensor(),transforms.Normalize((0.5,), (0.5,))])
        data_augmentations = transforms.Compose([transforms.ToPILImage(mode=None),transforms.RandomHorizontalFlip(p=0.5),transforms.RandomVerticalFlip(p=0.5),
                                                 transforms.ToTensor(),transforms.Normalize((0.5,), (0.5,))])
    #elif not isinstance(data_augmentations, transforms.Compose):
        #raise NotImplementedError
        
  
    train_dataset = data_dict[args.dataset](data_dir, True, data_augmentations)
    test_dataset = data_dict[args.dataset](data_dir, False, data_augmentations)

    # Make data batch iterable
    # Could modify the sampler to not uniformly random sample
    train_loader = DataLoader(dataset=train_dataset,
                              batch_size=batch_size,
                              shuffle=True)
    test_loader = DataLoader(dataset=test_dataset,
                             batch_size=batch_size,
                             shuffle=False)
   
    
    
    
    if load_model_str:
        model = torchModel(model_config,
                       input_shape=(
                           train_dataset.channels,
                           train_dataset.img_rows,
                           train_dataset.img_cols
                       ),
                       num_classes=10
            ).to(device)
        model.load_state_dict(torch.load(load_model_str))
        in_f = model.last_fc.in_features
        model.last_fc = nn.Linear(in_f, train_dataset.n_classes)
        model.dropout = nn.Dropout(p=0.2)
        """ # for Freezing layers
        count=0    
        for params in model.parameters():
            params.requires_grad=False
            count=count+1
            print(params.name)
            print(count)
            if count>2:
                params.requires_grad=True
               

            print(params.requires_grad)
        """    
        print('===============================')
        print('Loaded Pretrained model weights from KMNIST model',model)

    else:
        model = torchModel(model_config,
                       input_shape=(
                           train_dataset.channels,
                           train_dataset.img_rows,
                           train_dataset.img_cols
                       ),
                       num_classes=train_dataset.n_classes
            ).to(device)


    total_model_params = np.sum(p.numel() for p in model.parameters())
    

    model = model.to(device)
    # instantiate optimizer flexible to change for learning rate decay
    """ # Use when you opt for freezing the layers

    optimizer = model_optimizer(
        [{"params": model.conv_layers.parameters(), "lr": learning_rate/100},
         {"params": model.fc_layers.parameters(), "lr": learning_rate/10},
         {"params": model.last_fc.parameters(), "lr": learning_rate}],
        lr=learning_rate,)
    """    
    # instantiate training criterion
    optimizer= model_optimizer(model.parameters(),lr=learning_rate)
    train_criterion = train_criterion().to(device)
    logging.info('Generated Network:')
    summary(model, (train_dataset.channels,
                    train_dataset.img_rows,
                    train_dataset.img_cols),
            device='cuda' if torch.cuda.is_available() else 'cpu')

    testacc=[]
    trainacc=[]
    
    # Train the model

    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'max', factor=0.1, patience=1, verbose=True,threshold=1e-3)
    for epoch in range(num_epochs):
        logging.info('#' * 50)
        logging.info('Epoch [{}/{}]'.format(epoch + 1, num_epochs))
            
        train_score, train_loss = model.train_fn(optimizer, train_criterion,train_loader, device)
        trainacc.append(train_score)
        logging.info('Train accuracy %f', train_score)

        test_score = model.eval_fn(test_loader, device)
        testacc.append(test_score)
        logging.info('Test accuracy %f', test_score)
        scheduler.step(test_score)

    print(testacc)
    plt.plot(trainacc)
    plt.plot(testacc)
    plt.title('Model accuracy')
    if load_model_str:
        plt.suptitle('K49 on pretrained KMNIST')

    else:
        plt.suptitle(str(args.dataset))
        #plt.suptitle('K49 on default architecture')
    plt.xticks(np.arange(0,epoch+1,1))
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Test'], loc='upper left')
    plt.show()
    

    if save_model_str:
        # Save the model checkpoint can be restored via "model = torch.load(save_model_str)"
        if os.path.exists(save_model_str):
            save_model_str += '_'.join(time.ctime())
        torch.save(model.state_dict(), save_model_str)

    


if __name__ == '__main__':
    """
    This is just an example of how you can use train and evaluate
    to interact with the configurable network
    """
    loss_dict = {'cross_entropy': torch.nn.CrossEntropyLoss,
                 'mse': torch.nn.MSELoss}
    opti_dict = {'adam': torch.optim.Adam,
                 'adad': torch.optim.Adadelta,
                 'sgd': torch.optim.SGD}

    data_dict= {'K49' :K49,
                'KMNIST':KMNIST}

    cmdline_parser = argparse.ArgumentParser('AutoML SS19 final project')

    cmdline_parser.add_argument('-e', '--epochs',
                                default=10,
                                help='Number of epochs',
                                type=int)
    cmdline_parser.add_argument('-b', '--batch_size',
                                default=96,
                                help='Batch size',
                                type=int)
    cmdline_parser.add_argument('-D', '--data_dir',
                                default='../data',
                                help='Directory in which the data is stored (can be downloaded)')
    cmdline_parser.add_argument('-l', '--learning_rate',
                                default=0.01,
                                help='Optimizer learning rate',
                                type=float)
    cmdline_parser.add_argument('-L', '--training_loss',
                                default='cross_entropy',
                                help='Which loss to use during training',
                                choices=list(loss_dict.keys()),
                                type=str)
    cmdline_parser.add_argument('-o', '--optimizer',
                                default='adam',
                                help='Which optimizer to use during training',
                                choices=list(opti_dict.keys()),
                                type=str)
    cmdline_parser.add_argument('-m', '--model_path',
                                default=None,
                                help='Path to store model',
                                type=str)

    cmdline_parser.add_argument('-pretrained', '--trained_model_path',
                                default=None,
                                help='Path to load model',
                                type=str)
    
    cmdline_parser.add_argument('-da', '--data_augmentations',
                                default= False,
                                help='data augmentations',
                                type=bool)
    
    cmdline_parser.add_argument('-v', '--verbose',
                                default='INFO',
                                choices=['INFO', 'DEBUG'],
                                help='verbosity')

    cmdline_parser.add_argument('-data', '--dataset',
                                default= 'KMNIST',
                                help='which dataset to use',
                                choices=list(data_dict.keys()),
                                type=str)

    args, unknowns = cmdline_parser.parse_known_args()
    log_lvl = logging.INFO if args.verbose == 'INFO' else logging.DEBUG
    logging.basicConfig(level=log_lvl)

    if unknowns:
        logging.warning('Found unknown arguments!')
        logging.warning(str(unknowns))
        logging.warning('These will be ignored')

    # architecture parametrization
    architecture = {
        'n_layers':2,
        'n_conv_layers':2 ,
        'kernel_size':5,
        'out_channels' :21, 
        'n_out': 1284 
    }


    main(
        architecture,
        data_dir=args.data_dir,
        num_epochs=args.epochs,
        batch_size=args.batch_size,
        learning_rate=args.learning_rate,
        train_criterion=loss_dict[args.training_loss],
        model_optimizer=opti_dict[args.optimizer],
        data_augmentations=args.data_augmentations,
        save_model_str=args.model_path,
        load_model_str=args.trained_model_path
    )
