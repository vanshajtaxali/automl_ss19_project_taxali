import os
import argparse
import pickle



parser = argparse.ArgumentParser()
parser.add_argument("--all", action = "store_true", dest="show_all", default = False)
parser.add_argument("--loss", action = "store_true", dest="show_loss", default = False)
args = parser.parse_args()

if(args.show_all):
    X = open('All_configs', 'rb')
    all_config=pickle.load(X)
    print('All impletmented Settings----\n',all_config)

if(args.show_loss):
    Y = open('Best_Test_loss', 'rb')
    losses=pickle.load(Y)
    print('Best Test loss----\n',losses)

else:
    z = open('Best_config_augmented', 'rb')
    best_config=pickle.load(z)
    print('Best congiguration----\n',best_config)
