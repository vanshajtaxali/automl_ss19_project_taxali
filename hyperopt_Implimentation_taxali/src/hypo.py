import os
import argparse
import logging
import time
import numpy as np
import torch
import torchvision.transforms as transforms
from torch.utils.data import DataLoader
from torchsummary import summary
from hyperopt import fmin, tpe, rand, hp, space_eval, Trials
from cnn import torchModel
from datasets import K49,KMNIST
from glob import glob
import matplotlib.cm as cm
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from sklearn.preprocessing import StandardScaler
from tqdm import tqdm
import plotly.graph_objects as go
import plotly.offline
import progressbar
import pickle
import csv
import os



test_acc=[]
train_acc=[]
evaluations=[]
tes_l=[]
train_l=[]
all_settings=[]
all_architecture=[]
for_plot=[]
def main(model_config,
         data_dir='../data',
         num_epochs=10    ,
         batch_size=96,
         learning_rate=0.001,
         train_criterion=torch.nn.CrossEntropyLoss,
         model_optimizer=torch.optim.Adam,
         data_augmentations=False,
         save_model_str=None):
    """
    Training loop for configurableNet.
    :param model_config: network config (dict)
    :param data_dir: dataset path (str)
    :param num_epochs: (int)
    :param batch_size: (int)
    :param learning_rate: model optimizer learning rate (float)
    :param train_criterion: Which loss to use during training (torch.nn._Loss)
    :param model_optimizer: Which model optimizer to use during trainnig (torch.optim.Optimizer)
    :param data_augmentations: List of data augmentations to apply such as rescaling.
        (list[transformations], transforms.Composition[list[transformations]], None)
        If none only ToTensor is used
    :return:
    """
   
    # Device configuration
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

    if data_augmentations is False:
        # You can add any preprocessing/data augmentation you want here
        data_augmentations=transforms.ToTensor()
      
    elif data_augmentations is True:
        print('Data augmentation :TRUE')
        data_augmentations = transforms.Compose([transforms.ToPILImage(mode=None),transforms.ToTensor(),transforms.Normalize((0.5,), (0.5,))])

    train_dataset = data_dict[args.dataset](data_dir, True, data_augmentations)
    test_dataset = data_dict[args.dataset](data_dir, False, data_augmentations)


    # Make data batch iterable
    # Could modify the sampler to not uniformly random sample
    train_loader = DataLoader(dataset=train_dataset,
                              batch_size=batch_size,
                              shuffle=True)
    test_loader = DataLoader(dataset=test_dataset,
                             batch_size=batch_size,
                             shuffle=False)

    model = torchModel(model_config,
                       input_shape=(
                           train_dataset.channels,
                           train_dataset.img_rows,
                           train_dataset.img_cols
                       ),
                       num_classes=train_dataset.n_classes
                       ).to(device)
    total_model_params = np.sum(p.numel() for p in model.parameters())
    # instantiate optimizer
    optimizer = model_optimizer(model.parameters(),
                                lr=learning_rate)
    # instantiate training criterion
    train_criterion = train_criterion().to(device)


        
    logging.info('Generated Network:')
    summary(model, (train_dataset.channels,
                        train_dataset.img_rows,
                        train_dataset.img_cols),
            device='cuda' if torch.cuda.is_available() else 'cpu')
    print('Device used: {}'.format(device))


    

        
   
    # Train the model
   # for epoch in  progressbar.progressbar(range(num_epochs))
    for  epoch in range (num_epochs):
        train_score, train_loss = model.train_fn(optimizer, train_criterion,
                                                 train_loader, device)
        
        test_score, t_loss = model.eval_fn(test_loader, train_criterion, device)
        
    
    
    print('Train accuracy: {} | Train loss: {} '.format(train_score,train_loss))
        
    print('Test accuracy: {} | Test loss: {} '.format(test_score,t_loss))

    
    
        

    if save_model_str:
        # Save the model checkpoint can be restored via "model = torch.load(save_model_str)"
        if os.path.exists(save_model_str):
            save_model_str += '_'.join(time.ctime())
        torch.save(model.state_dict(), save_model_str)


    return t_loss, test_score, train_loss, train_score


def objective(args):
    
   
    num_epochs=int(args['param']['num_epochs'])
    batch_size=int(args['param']['batch_size'])
    learning_rate=args['param']['learning_rate']
    model_optimizer=args['param']['model_optimizer']
    train_criterion=torch.nn.CrossEntropyLoss
    n_layers =int(args['param']['n_layers'])
    n_conv_layers=int(args['param']['n_conv_layers'])
    kernel_size=int(args['param']['kernel_size'])
    out_channels=int(args['param']['out_channels'])
    n_out=int(args['param']['n_out'])
    drop =float(args['param']['drop'])
    # architecture parametrization
    architecture = {
        'n_layers': n_layers,
        'n_conv_layers': n_conv_layers,
        'kernel_size': kernel_size,
        'out_channels' : out_channels,
        'n_out': n_out,
        'drop' : drop
    }

    print ('........Generated Architecture Settings..........')
    for keys,values in architecture.items():
        print (keys + ' =>> ' + str(values))

    Settings_dict = {'Epochs: ' : num_epochs,
                     'Batch_size: ' : batch_size,
                     'Learning_rate: ' : learning_rate,
                     'Train criterion: ': train_criterion,
                     'Optimizer:  ': str(model_optimizer)}

    
    

    #print ("Generated settings: %s" % str(Settings_dict))
    print ('........Generated settings..........')
    for keys,values in Settings_dict.items():
        print (keys + str(values))
       
    x, y, z, a = main(architecture,
                data_dir='../data',
                num_epochs=num_epochs,    
                batch_size=batch_size,
                learning_rate=learning_rate,
                train_criterion=train_criterion,
                model_optimizer=model_optimizer,
                data_augmentations=True,
                save_model_str=None)
    
    tes_l.append(x)
    test_acc.append(y)
    train_l.append(z)
    train_acc.append(a)
    evaluations.append(len(test_acc))
    all_settings.append(Settings_dict)
    all_architecture.append(architecture)
    return z


if __name__ == '__main__':

    cmdline_parser = argparse.ArgumentParser('AutoML SS19 final project')
    
    algo_dict={'tpe': tpe.suggest,
            'random': rand.suggest}

    data_dict= {'K49' :K49,
                'KMNIST':KMNIST}

    
    print('Welcome to Auto_ML project Solution...')
    opt_eval = int(input('Enter the number of evaluations : '))

    cmdline_parser.add_argument('-algo', '--algorithm',
                                default='random',
                                help='Which algorithm to use for hyperopt',
                                choices=list(algo_dict.keys()),
                                type=str)
    cmdline_parser.add_argument('-data', '--dataset',
                                default= 'K49',
                                help='which dataset to use',
                                choices=list(data_dict.keys()),
                                type=str)
    
    args, unknowns = cmdline_parser.parse_known_args()

    
    loss_dict =(torch.nn.CrossEntropyLoss, torch.nn.MSELoss)
    opti_dict=(torch.optim.Adam, torch.optim.Adadelta, torch.optim.SGD)
    trials = Trials()
 
    best = fmin(objective,
                space={'param': {'num_epochs': hp.quniform('num_epochs',10,30,1),
                               'batch_size': hp.quniform('batch_size',70, 120, 1),
                               'learning_rate': hp.loguniform('learning_rate', np.log(0.00001), np.log(0.1)),
                               'model_optimizer': hp.choice('model_optimizer', opti_dict),
                                'n_layers' :  hp.quniform('n_layers',1,3,1),
                                'n_conv_layers': hp.quniform ('n_conv_layers',1,2,1),
                                'kernel_size':hp.uniform('kernel_size',5,10),
                                 'out_channels':hp.uniform('out_channels',20,30),
                                 'n_out': hp.quniform('n_out', 1024,2048,1),
                                  'drop': hp.quniform('drop', 0.5,0.8,0.1)}
                       },
                algo=algo_dict[args.algorithm],
                max_evals=opt_eval, trials=trials)


    best_l=[]
    best_l.append(best)
    

    print ('........Best Settings..........')
    for keys,values in best.items():
        print (keys + ' =>> ' + str(values))
        
########################### Writing data to files################################################

    print(test_acc[0],train_acc[0],all_settings[0],all_architecture[0])
    algorit_str=args.algorithm
    print(algorit_str)
    if algorit_str=='random':
        with open('hyperplot/test_accuracy_loss_RANDOM.csv', mode='w') as test_file:
            acc_writer = csv.writer(test_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            acc_writer.writerow([test_acc])
            acc_writer.writerow([tes_l])
    
        with open('hyperplot/train_accuracy_loss_RANDOM.csv', mode='w') as train_file:
            acc_writer = csv.writer(train_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            acc_writer.writerow([train_acc])
            acc_writer.writerow([train_l])


        keys = all_settings[0].keys()

        with open('hyperplot/settings_RANDOM.csv', mode='w') as settings_file:
            settings_writer = csv.DictWriter(settings_file, keys)
            settings_writer.writeheader()

            settings_writer.writerows(all_settings)


    
        keys = all_architecture[0].keys()

        with open('hyperplot/architecture_RANDOM.csv', mode='w') as arch_file:
            arch_writer = csv.DictWriter(arch_file, keys)
            arch_writer.writeheader()
            arch_writer.writerows(all_architecture)


        keys = best_l[0].keys()

        with open('hyperplot/best_RANDOM.csv', mode='w') as best_file:
            best_writer = csv.DictWriter(best_file, keys)
            best_writer.writeheader()
            best_writer.writerows(best_l)
            
    if algorit_str=='tpe':
        with open('hyperplot/test_accuracy_loss_TPE.csv', mode='w') as test_file:
            acc_writer = csv.writer(test_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            acc_writer.writerow([test_acc])
            acc_writer.writerow([tes_l])
    
        with open('hyperplot/train_accuracy_loss_TPE.csv', mode='w') as train_file:
            acc_writer = csv.writer(train_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            acc_writer.writerow([train_acc])
            acc_writer.writerow([train_l])


        keys = all_settings[0].keys()

        with open('hyperplot/settings_TPE.csv', mode='w') as settings_file:
            settings_writer = csv.DictWriter(settings_file, keys)
            settings_writer.writeheader()

            settings_writer.writerows(all_settings)


    
        keys = all_architecture[0].keys()

        with open('hyperplot/architecture_TPE.csv', mode='w') as arch_file:
            arch_writer = csv.DictWriter(arch_file, keys)
            arch_writer.writeheader()
            arch_writer.writerows(all_architecture)


        keys = best_l[0].keys()

        with open('hyperplot/best_TPE.csv', mode='w') as best_file:
            best_writer = csv.DictWriter(best_file, keys)
            best_writer.writeheader()
            best_writer.writerows(best_l)

########################Temporary Plot#####################################
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=evaluations, y=test_acc,
                    mode='lines+markers',
                    name='Test accuracy',
                    text=all_settings))
    fig.add_trace(go.Scatter(x=evaluations, y=train_acc,
                    mode='lines+markers',
                    name='Train accuracy',
                    text=all_architecture))

    
    if algorit_str=='random':
        fig.update_layout(title="Test & Train accuracy for Hyperplot using Random Search",
                  xaxis_title="Evaluations",
                  yaxis_title="Accuracy")
        plotly.offline.plot(fig, filename='random_KMNIST.html', image='png', auto_open=False)
        print('Random curve ploted')
    if algorit_str=='tpe':
        fig.update_layout(title="Test & Train accuracy for Hyperplot using Tree Parzen Estimator",
                  xaxis_title="Evaluations",
                  yaxis_title="Accuracy")
        plotly.offline.plot(fig, filename='tpe_KMNIST.html', image='png', auto_open=False)
        print('Tpe curve plotted')
