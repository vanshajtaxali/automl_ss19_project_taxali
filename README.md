### Due: 20.09.19 (23:59 CET)

# AutoML_SS19_project

Repository with the baseline code for the AutoML SS19 final project (https://ilias.uni-freiburg.de/ilias.php?ref_id=1264546&cmdClass=ilrepositorygui&cmdNode=vs&baseClass=ilrepositorygui)




How ro run the code ?
After installing the additional requirements go to the source folder and run hypo.py
The code will run on K49 data set with random search algorithm by default, if you want to change the dataset use argument: -data KMNIST for kmnist
to change algo to tpe use argument -algo tpe.

example: in src folder python3 hypo.py -algo tpe -data KMNIST
Enter the number of evaluations: -by user

after the code is executed it generates and interactive plot which will show you the configuration test and train accuracy at each evaluation with the configuration. 
Please change the name of the plot manually on basis of dataset you want to chose beforehand.
The file will be saved in .html format which will open in the browser and also providing download option todownload the plot in png format.
Apart from it the code returns some files in .csv format that contains accuracy, losses,architecture and settings in hyperplot folder with _corresponding algorithm.
Please change the name manually according to dataset beforehand.


From the plot pick a configuration  
go to project_original/src

run the main file and change learning rate batch size etc by default arguments given before
for architecture enter the parameters in architecture dictionary in end of the main file 
additional argumets include -data for dataset selection and -pretrained (model_path) to load KMNIST for using a pretrained model and  -da True for data augmentations

example: python3 main.py -l 0.01 -b 100 -e 20 -da True -pretrained model/KMNIST -o sgd -data K49  


The execution will open a plot of accuracy which you can save in any location 
The plots have been already added in Plots folder of the same locaton
